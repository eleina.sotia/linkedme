<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

/**
 * Class NetworkController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{

    public function index(Request $request)
    {
        $user = $request->user();

      return  Post::with('user')->where(function($query) use ($user){
          $query->whereHas('user.followers',function($query) use ($user){
              $query->where('id','=',$user->id);
          })->orWhere('user_id','=',$user->id);
      })->orderByDesc('posted_datetime')->paginate();

    }


    public function store(Request $request)
    {
        $user = $request->user();

        $input = $request->all();
        Validator::make($input, [
            'content' => ['required', 'string', 'max:2000'],
        ])->validateWithBag('storePost');


       Post::create([
            'user_id'=>$user->id,
            'content' => $input['content'],
            'posted_datetime' => now(),
        ]);


        session()->flash('flash.banner', 'Post Created Successfully');
        session()->flash('flash.bannerStyle', 'success');

        return response()->redirectTo(route('dashboard'));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(Post $post)
    {
        $this->authorize('delete',$post);


        $post->delete();

        return response('success');
    }

}