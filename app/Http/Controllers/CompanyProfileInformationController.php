<?php

namespace App\Http\Controllers;


use App\Models\JobArea;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyProfileInformationController extends Controller
{

    public function register(Request $request)
    {
        $user = $request->user();
        $user->load(['companyProfile']);

        if(blank($user->companyProfile))
        {
            $user->companyProfile()->create();
        }

        session()->flash('flash.banner', 'Successfully joined as company');
        session()->flash('flash.bannerStyle', 'success');

        return $request->wantsJson()
            ? new JsonResponse('', 200)
            : back()->with('status', 'company-profile-created');
    }

    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $companyProfile = $request->user()->companyProfile;

        $input = $request->all();
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'photo' => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
            'description'=>['nullable','max:2000','string'],
            'address'=>['nullable','string','max:500'],
            'website_url'=>['nullable','string','max:255','url'],
            'job_area_id'=>['nullable','exists:'.JobArea::class.',id','max:255'],


        ])->validateWithBag('updateProfileInformation');

        if (isset($input['photo'])) {
            $companyProfile->updateProfilePhoto($input['photo']);
        }

        $companyProfile->forceFill([
                'name' => $input['name'],
                'description' => $input['description'],
                'address' => $input['address'],
                'website_url' => $input['website_url'],
                'job_area_id' => $input['job_area_id'],
        ])->save();


        return $request->wantsJson()
            ? new JsonResponse('', 200)
            : back()->with('status', 'profile-information-updated');
    }

}