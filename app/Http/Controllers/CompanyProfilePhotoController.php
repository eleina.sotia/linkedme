<?php

namespace App\Http\Controllers;


use App\Models\JobArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyProfilePhotoController extends Controller
{

    /**
     * Delete the current user's profile photo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
       $request->user()->companyProfile->deleteProfilePhoto();

        return back(303)->with('status', 'profile-photo-deleted');
    }

}