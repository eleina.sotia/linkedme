<?php

namespace App\Http\Controllers;


use App\Models\JobArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JobAreaController extends Controller
{


    public function index()
    {
        return JobArea::get();
    }


}