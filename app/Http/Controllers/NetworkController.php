<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;

/**
 * Class NetworkController
 * @package App\Http\Controllers
 */
class NetworkController extends Controller
{

    public function index(Request $request)
    {
        return  User::with(['following','followers'])->where('id','!=',$request->user()->id)->paginate();
    }


    public function follow(Request $request,User $user)
    {
        $loggedInUser = $request->user();


        $user->followers()->syncWithoutDetaching([$loggedInUser->id]);

        $loggedInUser->following()->syncWithoutDetaching([$user->id]);




        return response('success');

    }

    public function unFollow(Request $request,User $user)
    {

        $loggedInUser = $request->user();


        $user->followers()->detach([$loggedInUser->id]);

        $loggedInUser->following()->detach([$user->id]);



        return response('success');
    }

}