<?php

namespace App\Http\Controllers;


use App\Models\JobArea;
use App\Models\JobListing;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class JobListingController extends Controller
{

    public function index(Request $request)
    {
      $query =   JobListing::query()->with(['user.companyProfile']);

      if((int)$request->get('onlyMine') === 1)
      {
          $query->where('user_id','=',$request->user()->id);
      }
      else
      {
          $query->where('start_date','<=',now()->toDateString())

              ->where('end_date','>=',now()->toDateString());
      }

      return $query->paginate();
    }

    public function show(JobListing $listing)
    {
        $listing->load(['user.companyProfile.jobArea']);

        return Inertia::render('JobListings/Show',[
            'listing'=>$listing,
        ]);
    }


    /**
     * @param JobListing $listing
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(JobListing $listing)
    {
        $this->authorize('update',$listing);

        $listing->load(['user.companyProfile.jobArea']);

        return Inertia::render('JobListings/Edit',[
            'listing'=>$listing,
        ]);
    }



    public function store(Request $request)
    {

        $user = $request->user();

        $input = $request->all();
        Validator::make($input, [
            'title' => ['required', 'string', 'max:255'],
            'short_description' => ['required', 'string', 'max:500'],
            'description'=>['nullable','max:2000','string'],
            'start_date'=>['required','date'],
            'end_date'=>['required','date','after:start_date'],
            'job_area_id'=>['required','exists:'.JobArea::class.',id','max:255'],


        ])->validateWithBag('storeJobListing');


        if($request->filled('id') && (int)$request->input('id') > 0)
        {
            $updateListing = JobListing::findOrFail($request->input('id'));

            $this->authorize('update',$updateListing);



            $updateListing->update([
                'title' => $input['title'],
                'short_description' => $input['short_description'],
                'description' => $input['description'],
                'start_date' => Carbon::parse($input['start_date'])->setTimezone(config('app.timezone'))->toDateTimeString(),
                'end_date' =>  Carbon::parse($input['end_date'])->setTimezone(config('app.timezone'))->toDateTimeString(),
                'job_area_id' => $input['job_area_id'],
            ]);


            session()->flash('flash.banner', 'Job Listing Updated Successfully');
            session()->flash('flash.bannerStyle', 'success');


        }
        else
        {
            JobListing::create([
                'user_id'=>$user->id,
                'title' => $input['title'],
                'short_description' => $input['short_description'],
                'description' => $input['description'],
                'start_date' => Carbon::parse($input['start_date'])->setTimezone(config('app.timezone'))->toDateTimeString(),
                'end_date' =>  Carbon::parse($input['end_date'])->setTimezone(config('app.timezone'))->toDateTimeString(),
                'job_area_id' => $input['job_area_id'],
            ])->save();


            session()->flash('flash.banner', 'Job Listing Created Successfully');
            session()->flash('flash.bannerStyle', 'success');

        }




        return response()->redirectTo(route('job'));
    }

    /**
     * @param JobListing $jobListing
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(JobListing $listing)
    {
        $this->authorize('delete',$listing);


        $listing->delete();

        return response('success');
    }


}