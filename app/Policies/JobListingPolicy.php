<?php

namespace App\Policies;

use App\Models\JobListing;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobListingPolicy
{
    use HandlesAuthorization;


    public function delete(User $user, JobListing $jobListing)
    {
        return $user->id === $jobListing->user_id;
    }

    public function update(User $user, JobListing $jobListing)
    {
        return $user->id === $jobListing->user_id;
    }
}
