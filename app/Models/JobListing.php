<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class JobListing extends Model
{

    use SoftDeletes;

    protected $table = 'job_listings';


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'title',
        'short_description',
        'description',
        'start_date',
        'end_date',
        'job_area_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }


    public function jobArea()
    {
        return $this->belongsTo(JobArea::class,'job_area_id');
    }


}
