<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpParser\Node\Expr\AssignOp\Mod;

class Post extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'user_id',
      'content',
      'posted_datetime',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}