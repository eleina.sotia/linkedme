<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {

    Route::get('/', function () {
        return Inertia::render('Dashboard');
    });
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('/job', function () {
        return Inertia::render('JobListings/Index');
    })->name('job');


    Route::get('/job/my-listings', function () {
        return Inertia::render('JobListings/MyListings');
    })->name('job.my.listings');

    Route::get('/job/create', function () {
        return Inertia::render('JobListings/Create');
    })->name('job.create');

    Route::post('/job-listing/store',[\App\Http\Controllers\JobListingController::class,'store'])->name('job.listing.store');
    Route::get('/job-listing/index',[\App\Http\Controllers\JobListingController::class,'index'])->name('job.listing.index');
    Route::get('/job-listing/show/{listing}',[\App\Http\Controllers\JobListingController::class,'show'])->name('job.listing.show');
    Route::get('/job-listing/edit/{listing}',[\App\Http\Controllers\JobListingController::class,'edit'])->name('job.listing.edit');

    Route::delete('/job-listing/delete/{listing}',[\App\Http\Controllers\JobListingController::class,'delete'])->name('job.listing.delete');


    Route::get('/post/index',[\App\Http\Controllers\PostController::class,'index'])->name('post.index');
    Route::put('/post/store',[\App\Http\Controllers\PostController::class,'store'])->name('post.store');
    Route::delete('/post/delete/{post}',[\App\Http\Controllers\PostController::class,'delete'])->name('post.delete');

    Route::get('/network', function () {
        return Inertia::render('Network/Index');
    })->name('network');

    Route::get('/network/index',[\App\Http\Controllers\NetworkController::class,'index'])->name('network.index');
    Route::post('/network/follow/{user}',[\App\Http\Controllers\NetworkController::class,'follow'])->name('network.follow');
    Route::post('/network/unfollow/{user}',[\App\Http\Controllers\NetworkController::class,'unFollow'])->name('network.unfollow');


    Route::post('/user/register-as-company',[\App\Http\Controllers\CompanyProfileInformationController::class,'register'])->name('user.register.as.company');

    Route::put('/company/profile-information', [\App\Http\Controllers\CompanyProfileInformationController::class, 'update'])
        ->name('company-profile-information.update');

    Route::delete('/company/profile-photo', [\App\Http\Controllers\CompanyProfilePhotoController::class, 'destroy'])
        ->name('current-user-company-photo.destroy');

    Route::get('job-areas',[\App\Http\Controllers\JobAreaController::class,'index'])->name('job.areas.index');

});
